package com.garrett.test;

import com.garrett.webengine.core.CoreServer;
import com.garrett.webengine.core.http.HttpResponse;
import com.garrett.webengine.core.template.Template;

public class Test {
	
	public static void main(String[] args) {
		int port = Integer.parseInt(args[0]);
		CoreServer server = new CoreServer(port);
		
		server.getRouter().get404(new HttpResponse("Cannot GET requested URL"));
		server.getRouter().post404(new HttpResponse("Cannot POST requested URL"));
		
		Template indexTemplate = new Template("templates/index.html");
		server.getRouter().get("/", new HttpResponse(indexTemplate));
		server.getRouter().post("/test", new HttpResponse("Form submitted!"));
		
		server.start();
	}
	
}
