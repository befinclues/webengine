package com.garrett.webengine.core.logging;

public class Errors {

	public static String E_PORT_NUMBER_INVALID = "Please enter a valid port number.";
	public static String E_SD_STREAM_FETCH = "Couldn't fetch socket descriptor input or output streams.";
	public static String E_INVALID_ROUTE_START = "Route string is invalid, please begin routes with a '/'.";
	
}
