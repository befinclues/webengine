package com.garrett.webengine.core.logging;

public class CoreLogger {

	public static void logRaw(String content, boolean brk) {
		if (brk) {
			System.out.println(content);
			return;
		}
		
		System.out.print(content);
	}
	
	public static void logDebug(String debug, boolean brk) {
		if (brk) {
			System.out.println("WebEngine Debug Message: " + debug);
			return;
		}
		
		System.out.print("WebEngine Debug Message: " + debug);
	}
	
	public static void logWarning(String warning, boolean brk) {
		if (brk) {
			System.out.println("WebEngine Warning: " + warning);
			return;
		}
		
		System.out.print("WebEngine Warning: " + warning);
	}
	
	public static void logError(String error, boolean brk) {
		if (brk) {
			System.err.println("WebEngine Error: " + error);
			return;
		}
		
		System.err.print("WebEngine Error: " + error);
	}
	
}
