package com.garrett.webengine.core.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.garrett.webengine.core.http.HeaderMap;
import com.garrett.webengine.core.logging.CoreLogger;
import com.garrett.webengine.core.logging.Errors;

public class CoreSocket {

	private Socket socket;
	private BufferedReader sdReader;
	private PrintWriter sdWriter;
	
	public CoreSocket(Socket socket) {
		this.socket = socket;
		
		try {
			sdReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			sdWriter = new PrintWriter(socket.getOutputStream());
		} catch (IOException e) {
			CoreLogger.logError(Errors.E_SD_STREAM_FETCH, true);
			e.printStackTrace();
		}
	}
	
	public void sendHeaders() {
		HeaderMap map = new HeaderMap();
		map.createHttpHeader();
		map.addHeader("Content-type", "text/html");
		map.addHeader("Connection", "close");
		send(map.getHeaderString());
	}
	
	public void sendHeaders(HeaderMap map) {
		send(map.getHeaderString());
	}
	
	public void send(String data) {
		sdWriter.print(data + "\r\n");
	}
	
	public void sendDebugResponse(String data) {
		send(data);	
		sdWriter.flush();
	}
	
	public String fetchRequest() {
		try {
			String requestHead = "";
			requestHead = sdReader.readLine();
			return requestHead;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		CoreLogger.logError("Couldn't read request from CoreSocket.", true);
		return null;
	}
	
	public String fetchFullRequest() {
		String request = "";
		try {
			while (sdReader.ready())
				request += sdReader.readLine() + "\r\n";
			
			return request;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		CoreLogger.logError("Couldn't read entire request from CoreSocket.", true);
		return null;
	}
	
	public Socket getJavaSocket() {
		return socket;
	}
	
	public void closeSocket() {
		sdWriter.close();
		try {
			sdReader.close();
			socket.close();
		} catch (IOException e) {
			CoreLogger.logError("Couldn't release socket properly.", true);
			e.printStackTrace();
		}
	}
	
}
