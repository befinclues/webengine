package com.garrett.webengine.core.network;

import java.io.IOException;
import java.net.ServerSocket;

import com.garrett.webengine.core.logging.CoreLogger;

public class ConnectionSocket {

	private int port;
	private ServerSocket socket;
	
	public ConnectionSocket(int port) {
		this.port = port;
		try {
			this.socket = new ServerSocket(port);
		} catch (IOException e) {
			CoreLogger.logError("Couldn't establish valid ConnectionSocket.", true);
			e.printStackTrace();
		}
	}
	
	public CoreSocket acceptNewSocket() {
		try {
			return new CoreSocket(socket.accept());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		CoreLogger.logError("Couldn't accept new valid connection to CoreSocket.", true);
		return null;
	}
	
	public int getPort() {
		return port;
	}
	
}
