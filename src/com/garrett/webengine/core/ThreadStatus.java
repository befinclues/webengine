package com.garrett.webengine.core;

public class ThreadStatus {

	private String currentStatus;
	private boolean valid;
	
	public ThreadStatus(String status, boolean initialValidity) {
		this.currentStatus = status;
		this.valid = initialValidity;
	}
	
	public void setStatus(String status) {
		this.currentStatus = status;
	}
	
	public String getCurrentStatus() {
		return currentStatus;
	}
	
	public void validate() {
		this.valid = true;
	}
	
	public void devalidate() {
		this.valid = false;
	}
	
	public boolean isValid() {
		return valid;
	}
	
}
