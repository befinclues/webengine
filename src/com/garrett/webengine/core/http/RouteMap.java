package com.garrett.webengine.core.http;

import java.util.ArrayList;
import java.util.List;

import com.garrett.webengine.core.logging.CoreLogger;

public class RouteMap {

	private List<RouteHead> routeHeads;
	private List<HttpResponse> routeBodies;
	
	public RouteMap() {
		routeHeads = new ArrayList<RouteHead>();
		routeBodies = new ArrayList<HttpResponse>();
	}
	
	public void add(RouteHead head, HttpResponse body) {
		if (routeHeads.contains(head)) {
			remove(head);
		}

		routeHeads.add(head);
		routeBodies.add(body);
	}
	
	public void remove(RouteHead head) {
		if (routeHeads.contains(head)) {
			int i = routeHeads.indexOf(head);
			routeHeads.remove(head);
			routeBodies.remove(i);
			return;
		}
		
		CoreLogger.logError("Cannot remove route " + head.getUrl() + "; it doesn't exist.", true);
		return;
	}
	
	public boolean contains(RouteHead head) {
		return routeHeads.contains(head);
	}
	
	public HttpResponse fetchRoute(RouteHead head) {
		System.out.println(head.getUrl());
		if (contains(head))
			return routeBodies.get(routeHeads.indexOf(head));
		
		return null;
	}
	
}
