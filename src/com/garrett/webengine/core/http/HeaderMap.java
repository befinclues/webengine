package com.garrett.webengine.core.http;

import java.util.ArrayList;
import java.util.List;

public class HeaderMap {

	private List<String> headers;
	
	public HeaderMap() {
		headers = new ArrayList<String>();
	}
	
	public void createHttpHeader() {
		addRawHeader("HTTP/1.1 200");
	}
	
	public void addRawHeader(String value) {
		headers.add(value + "\r\n");
	}
	
	public void addHeader(String label, String value) {
		addRawHeader(label + ": " + value);
	}
	
	public String getHeaderString() {
		String headerString = "";
		for (int i = 0; i < headers.size(); i++) {
			headerString += headers.get(i);
		}
		return headerString + "\r\n";
	}
	
}
