package com.garrett.webengine.core.http;

import java.util.HashMap;
import java.util.Scanner;

public class HttpRequest {

	private String requestString;
	private RouteHead routeHead;
	private HashMap<String, String> params;
	
	public HttpRequest(String requestString) {
		this.requestString = requestString;
		params = new HashMap<String, String>();
		parse();
	}
	
	private void parse() {
		Scanner s = new Scanner(requestString);
		
		String method = s.next();
		String url = s.next();
		if (url.contains("?")) url = url.substring(0, url.indexOf('?'));
		
		this.routeHead = new RouteHead(url, resolveMethodType(method));
	}
	
	public String getUrl() {
		return routeHead.getUrl();
	}
	
	public HttpRequestMethod getMethod() {
		return routeHead.getMethod();
	}
	
	public static HttpRequest resolveRequest(String requestString) {
		HttpRequestMethod method = new HttpRequest(requestString).getMethod();
		if (method == HttpRequestMethod.GET)
			return new HttpGetRequest(requestString);
		else if (method == HttpRequestMethod.POST)
			return new HttpGetRequest(requestString);
		else
			return new HttpRequest(requestString);
	}
	
	public static HttpRequestMethod resolveMethodType(String methodString) {
		switch (methodString) {
			case "GET":
				return HttpRequestMethod.GET;
				
			case "POST": 
				return HttpRequestMethod.POST;
			
			default: 
				return HttpRequestMethod.GET;
		}
	}
	
}
