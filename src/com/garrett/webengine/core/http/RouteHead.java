package com.garrett.webengine.core.http;

public class RouteHead {

	private String url;
	private HttpRequestMethod method;

	public RouteHead(String url) {
		this.url = url;
		this.method = HttpRequestMethod.GET;
	}
	
	public RouteHead(String url, HttpRequestMethod method) {
		this(url);
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HttpRequestMethod getMethod() {
		return method;
	}

	public void setMethod(HttpRequestMethod method) {
		this.method = method;
	}
	
	@Override
	public boolean equals(Object obj) {
		RouteHead rh = (RouteHead) obj;
		return (rh.getUrl().equals(url)) && (rh.getMethod() == method);
	}
	
}
