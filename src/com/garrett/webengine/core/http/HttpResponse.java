package com.garrett.webengine.core.http;

import com.garrett.webengine.core.template.Template;

public class HttpResponse {
	
	private String responseString;
	
	public HttpResponse() {
		this.responseString = "";
	}
	
	public HttpResponse(Template template) {
		this.responseString = template.getData();
	}
	
	public HttpResponse(String responseString) {
		this.responseString = responseString;
	}
	
	public String getResponseString() {
		return responseString;
	}
	
}
