package com.garrett.webengine.core.http;

import com.garrett.webengine.core.logging.CoreLogger;
import com.garrett.webengine.core.logging.Errors;

public class Router {
	
	private RouteMap routes;	
	private HttpResponse[] list404;
	
	public Router() {
		routes = new RouteMap();
		list404 = new HttpResponse[2];
		
		routes.add(new RouteHead("/favicon.ico"), new HttpResponse("icon"));
	}
	
	public void get(String url, HttpResponse response) {
		if (url.charAt(0) != '/') {
			CoreLogger.logError(Errors.E_INVALID_ROUTE_START, true);
			return;
		}
			
		routes.add(new RouteHead(url), response);
	}
	
	public void post(String url, HttpResponse response) {
		routes.add(new RouteHead(url, HttpRequestMethod.POST), response);
	}
	
	public void get404(HttpResponse response) {
		list404[0] = response;
	}
	
	public void post404(HttpResponse response) {
		list404[1] = response;
	}
	
	public HttpResponse fetchResponse(RouteHead head) {		
		return routes.fetchRoute(head);
	}
	
	public HttpResponse return_get_404() {
		return list404[0];
	}
	
	public HttpResponse return_post_404() {
		return list404[1];
	}
	
}
