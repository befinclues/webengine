package com.garrett.webengine.core;

import com.garrett.webengine.core.http.HttpRequest;
import com.garrett.webengine.core.http.HttpRequestMethod;
import com.garrett.webengine.core.http.HttpResponse;
import com.garrett.webengine.core.http.RouteHead;
import com.garrett.webengine.core.http.Router;
import com.garrett.webengine.core.logging.CoreLogger;
import com.garrett.webengine.core.logging.Errors;
import com.garrett.webengine.core.network.ConnectionSocket;
import com.garrett.webengine.core.network.CoreSocket;
import com.garrett.webengine.core.template.Template;

public class CoreServer implements Runnable {

	private CoreThread thread;
	private ConnectionSocket connectionSocket;
	private int port;
	
	private Router router;
	
	public CoreServer(int port) {
		if (port > 0)
			this.port = port;
		else
			CoreLogger.logError(Errors.E_PORT_NUMBER_INVALID, true);
		
		this.thread = new CoreThread(this, "WebEngine Thread", new ThreadStatus("running", false));
		this.connectionSocket = new ConnectionSocket(port);
		this.router = new Router();
	}
	
	public void start() {
		thread.getThreadStatus().validate();
		thread.start();
	}
	
	public void stop() {
		try {
			thread.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		while (thread.getThreadValidity()) {
			CoreSocket hSocket = connectionSocket.acceptNewSocket();
			hSocket.sendHeaders();
			
			String req = hSocket.fetchRequest();
			String requestBody = hSocket.fetchFullRequest();
			HttpRequest request = new HttpRequest(req);
			
			CoreLogger.logRaw(requestBody, true);
			
			Template temp = new Template("templates/index.html");
			String res = temp.getData();
			
			HttpResponse finalResponse = new HttpResponse();
			HttpRequestMethod reqType = request.getMethod();
			switch (reqType) {
				case GET: {
					RouteHead route = new RouteHead(request.getUrl(), HttpRequestMethod.GET);
					HttpResponse validResponse;
					if ((validResponse = router.fetchResponse(route)) != null) {
						finalResponse = validResponse;
						break;
					}
					
					finalResponse = router.return_get_404();
				} break;
					
				case POST: {
					RouteHead route = new RouteHead(request.getUrl(), HttpRequestMethod.POST);
					HttpResponse validResponse;
					if ((validResponse = router.fetchResponse(route)) != null) {
						finalResponse = validResponse;
						break;
					}
					
					finalResponse = router.return_post_404();
				} break;
			}
			
//			CoreLogger.logRaw(req, true);
			
			hSocket.sendDebugResponse(finalResponse.getResponseString());
			hSocket.closeSocket();
		}	
		
		stop();
	}

	public int getPort() {
		return port;
	}
	
	public Router getRouter() {
		return router;
	}
	
}
