package com.garrett.webengine.core;

public class CoreThread extends Thread {

	private Runnable threadHost;
	private String threadName;
	private ThreadStatus threadStatus;
	
	public CoreThread(Runnable threadHost, String threadName, ThreadStatus threadStatus) {
		super(threadHost, threadName);
		
		this.threadHost = threadHost;
		this.threadStatus = threadStatus;
		this.threadName = threadName;
	}

	public void setThreadStatus(String status) {
		threadStatus.setStatus(status);
	}
	
	@Override
	public void start() {
		super.start();
	}
	
	public void release() throws InterruptedException {
		threadStatus.devalidate();
		super.join();
	}

	public ThreadStatus getThreadStatus() {
		return threadStatus;
	}
	
	public boolean getThreadValidity() {
		return threadStatus.isValid();
	}

	public Runnable getThreadHost() {
		return threadHost;
	}

	public String getThreadName() {
		return threadName;
	}
	
}
