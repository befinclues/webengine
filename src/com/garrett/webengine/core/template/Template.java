package com.garrett.webengine.core.template;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import com.garrett.webengine.core.logging.CoreLogger;

public class Template {

	private String path;
	private String data = "";
	
	private HashMap<String, String> context = new HashMap<String, String>(); // Context for templating
	
	public Template(String path) {
		this.path = path;
		try {
			FileInputStream stream = new FileInputStream(path);
			Scanner s = new Scanner(stream);
			
			while (s.hasNextLine()) {
				data += s.nextLine();
			}
		} catch (FileNotFoundException e) {
			CoreLogger.logError("Couldn't open template " + path, true);
			e.printStackTrace();
		}
	}
	
	public String getData() {
		fillContext();
		return data;
	}
	
	private void fillContext() {
		// Search for variables
		// Fill in variables
	}
	
}
